proxy:
	ansible-playbook -i hosts deploy-proxy.yml
swarm:
	ansible-playbook -i hosts deploy-swarm.yml
gluster:
	ansible-playbook -i hosts gluster.yml
swarm_only:
	ansible-playbook -i hosts swarm.yml
pt:
	ansible-playbook -i hosts portainer-traefik.yml
